/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.pages.taskbase.top;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 *
 * @author Yamashita,Takahiro
 */
public class TopPage {

    public void fwSearchPage() {
        open("http://localhost:8082/jsf-ddd/");
        $("#f-command1").click();
    }

    public void fwSearchPageTargetMessage() {
        open("http://localhost:8082/jsf-ddd/");
        $("#f-command2").click();
    }

    public void fwSearchErrorStyle() {
        open("http://localhost:8082/jsf-ddd/");
        $("#f-command3").click();
    }

}
