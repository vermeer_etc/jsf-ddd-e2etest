/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.pages.taskbase;

import static com.codeborne.selenide.Selenide.$;
import com.codeborne.selenide.SelenideElement;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ConfirmPage {

    public SelenideElement userEmail = $("#email");

    public SelenideElement userName = $("#name");

    public SelenideElement dateOfBirth = $("#dateOfBirth");

    public SelenideElement phoneNumber = $("#phoneNumber");

    public SelenideElement gender = $("#gender");

    public SelenideElement registerButton = $("#registerButton");

    public SelenideElement modifyLink = $("#f-modify");

    public void fwComplete() {
        registerButton.click();
    }

    public void modify() {
        modifyLink.click();
    }

}
