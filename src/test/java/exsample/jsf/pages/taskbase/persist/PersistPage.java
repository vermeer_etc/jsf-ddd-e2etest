/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.pages.taskbase.persist;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import com.codeborne.selenide.SelenideElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;

/**
 *
 * @author Yamashita,Takahiro
 */
public class PersistPage {

    public SelenideElement userEmail = $("#f-email");

    public SelenideElement userName = $("#f-name");

    public SelenideElement dateOfBirth = $("#f-dateOfBirth");

    public SelenideElement phoneNumber = $("#f-phoneNumber");

    public SelenideElement gender = $(By.name("gender"));

    public SelenideElement confirmButton = $("#f-confirmButton");

    public SelenideElement backSearchLink = $("#f-fwSearchPage");

    public ElementsCollection errorMessages = $$(".error-message");

    public ElementsCollection errorFields = $$(".error-field");

    public void fwConfirmPage() {
        this.confirmButton.click();
    }

    public void backSearchPage() {
        this.backSearchLink.click();
    }

    public boolean hasErrorMessage(String expectMessage) {
        Iterator<SelenideElement> iterator = errorMessages.iterator();

        List<String> messages = new ArrayList<>();
        while (iterator.hasNext()) {
            SelenideElement element = iterator.next();
            messages.add(element.text());
        }

        return messages.stream().anyMatch(message -> message.equals(expectMessage));
    }

    public boolean hasErrorMessage(SelenideElement targetElement, String expectMessage) {
        SelenideElement element = targetElement.closest("div").find(".error-message");
        return expectMessage.equals(element.getText());
    }

    public boolean hasErrorField(SelenideElement targetElement) {
        String name = targetElement.name();
        return errorFields.filter(Condition.name(name)).isEmpty() == false;
    }

    public boolean hasErrorStyle(SelenideElement targetElement) {
        return targetElement.parent().attr("class").contains("error");
    }

    public String tooltip(SelenideElement targetElement) {
        String _tooltip = targetElement.getAttribute("title");
        return (_tooltip == null)
               ? ""
               : _tooltip;
    }

}
