/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.pages.taskbase.search;

import com.codeborne.selenide.ElementsCollection;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

/**
 *
 * @author Yamashita,Takahiro
 */
public class SearchPage {

    private final SelenideElement persistButton = $("#f-persistButton");

    ElementsCollection userEmails = $$("[id^=f-items-][id$=email]");
    ElementsCollection userNames = $$("[id^=f-items-][id$=name]");

    public void fwPersistPage() {
        persistButton.click();
    }

    public boolean hasUser(String userEmail, String userName) {
        return userEmails.texts().stream().anyMatch(e -> e.equals(userEmail))
               && userNames.texts().stream().anyMatch(e -> e.equals(userName));
    }

    public void fwUpdatePageByRowIndex(Integer rowIndex) {
        String rowId = "f-items-" + rowIndex.toString() + "-updateButton";
        SelenideElement updateButton = $(By.id(rowId));
        updateButton.click();
    }

    public void fwRemovePageByRowIndex(Integer rowIndex) {
        String rowId = "f-items-" + rowIndex.toString() + "-removeButton";
        SelenideElement removeButton = $(By.id(rowId));
        removeButton.click();
    }

}
