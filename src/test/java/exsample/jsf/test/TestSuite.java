/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package exsample.jsf.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Yamashita,Takahiro
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    exsample.jsf.test.cases.UserRegistration.class,
    exsample.jsf.test.cases.PageNavigation.class,
    exsample.jsf.test.cases.UserRegistrationTargetMessage.class,
    exsample.jsf.test.cases.ErrorFieldColor.class,
    exsample.jsf.test.cases.UserRegistrationErrorStyle.class,
    exsample.jsf.test.cases.ErrorTooltip.class,})
public class TestSuite {

}
