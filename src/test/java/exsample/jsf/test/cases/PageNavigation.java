/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.test.cases;

import static com.codeborne.selenide.Condition.value;
import com.codeborne.selenide.SelenideElement;
import exsample.jsf.config.SelenideConfig;
import exsample.jsf.pages.taskbase.ConfirmPage;
import exsample.jsf.pages.taskbase.RemoveConfirmPage;
import exsample.jsf.pages.taskbase.persist.PersistPage;
import exsample.jsf.pages.taskbase.search.SearchPage;
import exsample.jsf.pages.taskbase.top.TopPage;
import exsample.jsf.pages.taskbase.update.UpdatePage;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 前のページに戻る、トップページに戻るといった操作フローのテスト
 *
 * @author Yamashita,Takahiro
 */
public class PageNavigation {

    @BeforeClass
    public static void setUp() {
        SelenideConfig.setUp();
    }

    @Test
    public void persist_backTop() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "33@example.com";
        String userName = "11 11";
        String dateOfBirth = "2001-01-01";
        String phoneNumber = "03-1234-5678";
        String gender = "MAN";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.backSearchPage();

        Assert.assertFalse(searchPage.hasUser(userEmail, userName));

    }

    @Test
    public void persist_confirm_backEdit() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "22@example.com";
        String userName = "22 22";
        String dateOfBirth = "2002-02-02";
        String phoneNumber = "03-2222-2222";
        String gender = "MAN";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ConfirmPage confirmPage = new ConfirmPage();
        confirmPage.modify();

        persistPage.userEmail.shouldBe(value(userEmail));
        persistPage.userName.shouldBe(value(userName));
        persistPage.dateOfBirth.shouldBe(value(dateOfBirth));
        persistPage.phoneNumber.shouldBe(value(phoneNumber));
        persistPage.gender.shouldBe(value(gender));

    }

    @Test
    public void update_backTop() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "aaaaaaZZ@example.com";

        UpdatePage updatePage = new UpdatePage();
        String beforeUserEmail = updatePage.userEmail.getValue();
        String beforeUserName = updatePage.userName.getValue();

        updatePage.userEmail.val(userEmail);
        updatePage.backSearchPage();

        Assert.assertTrue(searchPage.hasUser(beforeUserEmail, beforeUserName));
        Assert.assertFalse(searchPage.hasUser(userEmail, beforeUserName));

    }

    @Test
    public void update_confirm_backTop() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "aaaaaa33@example.com";
        String userName = "ＡＡ　ＢＢ";
        String dateOfBirth = "1980-04-11";
        String phoneNumber = "03-5678-1234";
        String gender = "WOMAN";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.userName.val(userName);
        updatePage.dateOfBirth.val(dateOfBirth);
        updatePage.phoneNumber.val(phoneNumber);
        updatePage.gender.selectRadio(gender);
        updatePage.fwConfirmPage();

        ConfirmPage confirmPage = new ConfirmPage();
        confirmPage.modify();

        updatePage.userEmail.shouldBe(value(userEmail));
        updatePage.userName.shouldBe(value(userName));
        updatePage.dateOfBirth.shouldBe(value(dateOfBirth));
        updatePage.phoneNumber.shouldBe(value(phoneNumber));
        SelenideElement selectedGender = updatePage.selectedGenderValue();
        selectedGender.shouldBe(value(gender));

    }

    @Test
    public void remove_backTop() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwRemovePageByRowIndex(0);

        RemoveConfirmPage confirmPage = new RemoveConfirmPage();
        confirmPage.backTopPage();

    }

}
