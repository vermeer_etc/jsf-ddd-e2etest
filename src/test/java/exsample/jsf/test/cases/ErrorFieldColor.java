/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.test.cases;

import com.codeborne.selenide.ElementsCollection;
import exsample.jsf.config.SelenideConfig;
import exsample.jsf.pages.taskbase.persist.PersistPage;
import exsample.jsf.pages.taskbase.search.SearchPage;
import exsample.jsf.pages.taskbase.top.TopPage;
import exsample.jsf.pages.taskbase.update.UpdatePage;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import static org.junit.Assert.assertThat;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * エラーとなった入力フィールドの背景色の変更を確認するテストです。
 *
 *
 * @author Yamashita,Takahiro
 */
public class ErrorFieldColor {

    @BeforeClass
    public static void setUp() {
        SelenideConfig.setUp();
    }

    @Test
    public void persistNewUserNonHtmlMessage() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "";
        String userName = "";
        String dateOfBirth = "";
        String phoneNumber = "";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ElementsCollection errorFields = persistPage.errorFields;
        errorFields.shouldHaveSize(4);
        Assert.assertTrue(persistPage.hasErrorField(persistPage.userEmail));
        Assert.assertTrue(persistPage.hasErrorField(persistPage.userName));
        Assert.assertTrue(persistPage.hasErrorField(persistPage.dateOfBirth));
        Assert.assertTrue(persistPage.hasErrorField(persistPage.phoneNumber));
    }

    @Test
    public void persistSameEmailUserNonHtmlMessage() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "aaaaaa@example.com";
        String userName = "11 11";
        String dateOfBirth = "2001-01-01";
        String phoneNumber = "03-1234-5678";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ElementsCollection errorMessages = persistPage.errorMessages;
        assertThat(errorMessages.size(), is(1));

        Assert.assertTrue(persistPage.hasErrorField(persistPage.userEmail));
        Assert.assertFalse(persistPage.hasErrorField(persistPage.userName));
        Assert.assertFalse(persistPage.hasErrorField(persistPage.dateOfBirth));
        Assert.assertFalse(persistPage.hasErrorField(persistPage.phoneNumber));
    }

    @Test
    public void updateNewUserFieldBlankNonHtmlMessage() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "";
        String userName = "";
        String dateOfBirth = "";
        String phoneNumber = "";
        String gender = "WOMAN";
        String genderValue = "女性";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.userName.val(userName);
        updatePage.dateOfBirth.val(dateOfBirth);
        updatePage.phoneNumber.val(phoneNumber);
        updatePage.gender.selectRadio(gender);
        updatePage.fwConfirmPage();

        ElementsCollection errorMessages = updatePage.errorMessages;
        assertThat(errorMessages.size(), is(4));

        Assert.assertTrue(updatePage.hasErrorField(updatePage.userEmail));
        Assert.assertTrue(updatePage.hasErrorField(updatePage.userName));
        Assert.assertTrue(updatePage.hasErrorField(updatePage.dateOfBirth));
        Assert.assertTrue(updatePage.hasErrorField(updatePage.phoneNumber));

    }

    @Test
    public void updateSameEmailUserNonHtmlMessage() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "bbbbbb@example.com";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.fwConfirmPage();

        ElementsCollection errorMessages = updatePage.errorMessages;
        assertThat(errorMessages.size(), is(1));

        Assert.assertTrue(updatePage.hasErrorField(updatePage.userEmail));
        Assert.assertFalse(updatePage.hasErrorField(updatePage.userName));
        Assert.assertFalse(updatePage.hasErrorField(updatePage.dateOfBirth));
        Assert.assertFalse(updatePage.hasErrorField(updatePage.phoneNumber));

    }

    @Test
    public void persistNewUserHtmlMessage() {
        new TopPage().fwSearchPageTargetMessage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "";
        String userName = "";
        String dateOfBirth = "";
        String phoneNumber = "";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ElementsCollection errorFields = persistPage.errorFields;
        errorFields.shouldHaveSize(4);
        Assert.assertTrue(persistPage.hasErrorField(persistPage.userEmail));
        Assert.assertTrue(persistPage.hasErrorField(persistPage.userName));
        Assert.assertTrue(persistPage.hasErrorField(persistPage.dateOfBirth));
        Assert.assertTrue(persistPage.hasErrorField(persistPage.phoneNumber));
    }

    @Test
    public void persistSameEmailUserHtmlMessage() {
        new TopPage().fwSearchPageTargetMessage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "aaaaaa@example.com";
        String userName = "11 11";
        String dateOfBirth = "2001-01-01";
        String phoneNumber = "03-1234-5678";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ElementsCollection errorMessages = persistPage.errorMessages;
        assertThat(errorMessages.size(), is(1));

        Assert.assertTrue(persistPage.hasErrorField(persistPage.userEmail));
        Assert.assertFalse(persistPage.hasErrorField(persistPage.userName));
        Assert.assertFalse(persistPage.hasErrorField(persistPage.dateOfBirth));
        Assert.assertFalse(persistPage.hasErrorField(persistPage.phoneNumber));
    }

    @Test
    public void updateNewUserFieldBlankHtmlMessage() {
        new TopPage().fwSearchPageTargetMessage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "";
        String userName = "";
        String dateOfBirth = "";
        String phoneNumber = "";
        String gender = "WOMAN";
        String genderValue = "女性";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.userName.val(userName);
        updatePage.dateOfBirth.val(dateOfBirth);
        updatePage.phoneNumber.val(phoneNumber);
        updatePage.gender.selectRadio(gender);
        updatePage.fwConfirmPage();

        ElementsCollection errorMessages = updatePage.errorMessages;
        assertThat(errorMessages.size(), is(4));

        Assert.assertTrue(updatePage.hasErrorField(updatePage.userEmail));
        Assert.assertTrue(updatePage.hasErrorField(updatePage.userName));
        Assert.assertTrue(updatePage.hasErrorField(updatePage.dateOfBirth));
        Assert.assertTrue(updatePage.hasErrorField(updatePage.phoneNumber));

    }

    @Test
    public void updateSameEmailUserHtmlMessage() {
        new TopPage().fwSearchPageTargetMessage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "bbbbbb@example.com";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.fwConfirmPage();

        ElementsCollection errorMessages = updatePage.errorMessages;
        assertThat(errorMessages.size(), is(1));

        Assert.assertTrue(updatePage.hasErrorField(updatePage.userEmail));
        Assert.assertFalse(updatePage.hasErrorField(updatePage.userName));
        Assert.assertFalse(updatePage.hasErrorField(updatePage.dateOfBirth));
        Assert.assertFalse(updatePage.hasErrorField(updatePage.phoneNumber));

    }

}
