/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.test.cases;

import static com.codeborne.selenide.Condition.text;
import com.codeborne.selenide.ElementsCollection;
import static com.codeborne.selenide.Selenide.*;
import exsample.jsf.config.SelenideConfig;
import exsample.jsf.pages.taskbase.CompletePage;
import exsample.jsf.pages.taskbase.ConfirmPage;
import exsample.jsf.pages.taskbase.RemoveConfirmPage;
import exsample.jsf.pages.taskbase.persist.PersistPage;
import exsample.jsf.pages.taskbase.search.SearchPage;
import exsample.jsf.pages.taskbase.top.TopPage;
import exsample.jsf.pages.taskbase.update.UpdatePage;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import static org.junit.Assert.assertThat;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class UserRegistration {

    @BeforeClass
    public static void setUp() {
        SelenideConfig.setUp();
    }

    @Test
    public void fwSearchPage() {
        new TopPage().fwSearchPage();
        $("h1").shouldHave(text("利用者一覧"));
    }

    @Test
    public void persistNewUser() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "11@example.com";
        String userName = "11 11";
        String dateOfBirth = "2001-01-01";
        String phoneNumber = "03-1234-5678";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ConfirmPage confirmPage = new ConfirmPage();
        confirmPage.userEmail.shouldBe(text(userEmail));
        confirmPage.userName.shouldBe(text(userName));
        confirmPage.dateOfBirth.shouldBe(text(dateOfBirth));
        confirmPage.phoneNumber.shouldBe(text(phoneNumber));
        confirmPage.gender.shouldBe(text(genderValue));
        confirmPage.fwComplete();

        CompletePage completePage = new CompletePage();
        completePage.completeMessage.shouldHave(text(userName + "(" + userEmail + ")"));
        completePage.fwSearchPage();

    }

    @Test
    public void updateNewUser() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "aaaaaa22@example.com";

        String dateOfBirth = "1980-04-11";
        String phoneNumber = "03-5678-1234";
        String gender = "WOMAN";
        String genderValue = "女性";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);

        updatePage.dateOfBirth.val(dateOfBirth);
        updatePage.phoneNumber.val(phoneNumber);
        updatePage.gender.selectRadio(gender);
        updatePage.fwConfirmPage();

        String userName = "ＡＡ　ＡＡ";

        ConfirmPage confirmPage = new ConfirmPage();
        confirmPage.userEmail.shouldBe(text(userEmail));
        confirmPage.userName.shouldBe(text(userName));
        confirmPage.dateOfBirth.shouldBe(text(dateOfBirth));
        confirmPage.phoneNumber.shouldBe(text(phoneNumber));
        confirmPage.gender.shouldBe(text(genderValue));
        confirmPage.fwComplete();

        CompletePage completePage = new CompletePage();
        completePage.completeMessage.shouldHave(text(userName + "(" + userEmail + ")"));
        completePage.fwSearchPage();

    }

    @Test
    public void removeNewUser() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwRemovePageByRowIndex(2);

        String userEmail = "cccccc@example.com";
        String userName = "ＣＣ　ＣＣ";

        RemoveConfirmPage confirmPage = new RemoveConfirmPage();
        confirmPage.userEmail.shouldBe(text(userEmail));
        confirmPage.userName.shouldBe(text(userName));
        confirmPage.fwComplete();

        CompletePage completePage = new CompletePage();
        completePage.completeMessage.shouldHave(text(userName + "(" + userEmail + ")"));
        completePage.fwSearchPage();

    }

    @Test
    public void persistNewUserFieldBlank() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "";
        String userName = "";
        String dateOfBirth = "";
        String phoneNumber = "";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ElementsCollection errorMessages = persistPage.errorMessages;
        assertThat(errorMessages.size(), is(4));

        Assert.assertTrue(persistPage.hasErrorMessage("生年月日 は入力必須です"));
        Assert.assertTrue(persistPage.hasErrorMessage("名前 は入力必須です"));
        Assert.assertTrue(persistPage.hasErrorMessage("電話番号 は入力必須です"));
        Assert.assertTrue(persistPage.hasErrorMessage("利用者ID は入力必須です"));

    }

    @Test
    public void updateNewUserFieldBlank() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "";
        String userName = "";
        String dateOfBirth = "";
        String phoneNumber = "";
        String gender = "WOMAN";
        String genderValue = "女性";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.userName.val(userName);
        updatePage.dateOfBirth.val(dateOfBirth);
        updatePage.phoneNumber.val(phoneNumber);
        updatePage.gender.selectRadio(gender);
        updatePage.fwConfirmPage();

        ElementsCollection errorMessages = updatePage.errorMessages;
        assertThat(errorMessages.size(), is(4));

        Assert.assertTrue(updatePage.hasErrorMessage("生年月日 は入力必須です"));
        Assert.assertTrue(updatePage.hasErrorMessage("名前 は入力必須です"));
        Assert.assertTrue(updatePage.hasErrorMessage("電話番号 は入力必須です"));
        Assert.assertTrue(updatePage.hasErrorMessage("利用者ID は入力必須です"));

    }

    @Test
    public void persistSameEmailUser() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "aaaaaa@example.com";
        String userName = "11 11";
        String dateOfBirth = "2001-01-01";
        String phoneNumber = "03-1234-5678";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        ElementsCollection errorMessages = persistPage.errorMessages;
        assertThat(errorMessages.size(), is(1));

        Assert.assertTrue(persistPage.hasErrorMessage("既に同一Emailは登録されています。"));
    }

    @Test
    public void updateSameEmailUser() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "bbbbbb@example.com";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.fwConfirmPage();

        ElementsCollection errorMessages = updatePage.errorMessages;
        assertThat(errorMessages.size(), is(1));

        Assert.assertTrue(updatePage.hasErrorMessage("既に同一Emailは登録されています。"));
    }

    @Test
    public void updateSameEmailUserNoChangeEmail() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwUpdatePageByRowIndex(0);

        String userEmail = "aaaaaa@example.com";
        String userName = "ＡＡ　ＡＡ";

        UpdatePage updatePage = new UpdatePage();
        updatePage.userEmail.val(userEmail);
        updatePage.fwConfirmPage();

        ConfirmPage confirmPage = new ConfirmPage();
        confirmPage.fwComplete();

        CompletePage completePage = new CompletePage();
        completePage.completeMessage.shouldHave(text(userName + "(" + userEmail + ")"));
        completePage.fwSearchPage();

    }

}
