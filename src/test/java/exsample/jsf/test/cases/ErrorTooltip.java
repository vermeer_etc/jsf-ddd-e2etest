/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2018 Yamashita,Takahiro
 */
package exsample.jsf.test.cases;

import exsample.jsf.config.SelenideConfig;
import exsample.jsf.pages.taskbase.persist.PersistPage;
import exsample.jsf.pages.taskbase.search.SearchPage;
import exsample.jsf.pages.taskbase.top.TopPage;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * エラーとなった入力フィールドの背景色の変更を確認するテストです。
 *
 *
 * @author Yamashita,Takahiro
 */
public class ErrorTooltip {

    @BeforeClass
    public static void setUp() {
        SelenideConfig.setUp();
    }

    @Test
    public void persistNewUserNonHtmlMessage() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "";
        String userName = "";
        String dateOfBirth = "";
        String phoneNumber = "";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        Assert.assertEquals("利用者ID は入力必須です", persistPage.tooltip(persistPage.userEmail));
        Assert.assertEquals("名前 は入力必須です", persistPage.tooltip(persistPage.userName));
        Assert.assertEquals("生年月日 は入力必須です", persistPage.tooltip(persistPage.dateOfBirth));
        Assert.assertEquals("電話番号 は入力必須です", persistPage.tooltip(persistPage.phoneNumber));
    }

    @Test
    public void persistSameEmailUserNonHtmlMessage() {
        new TopPage().fwSearchPage();
        SearchPage searchPage = new SearchPage();
        searchPage.fwPersistPage();

        String userEmail = "aaaaaa@example.com";
        String userName = "11 11";
        String dateOfBirth = "2001-01-01";
        String phoneNumber = "03-1234-5678";
        String gender = "MAN";
        String genderValue = "男性";

        PersistPage persistPage = new PersistPage();
        persistPage.userEmail.val(userEmail);
        persistPage.userName.val(userName);
        persistPage.dateOfBirth.val(dateOfBirth);
        persistPage.phoneNumber.val(phoneNumber);
        persistPage.gender.selectRadio(gender);
        persistPage.fwConfirmPage();

        Assert.assertEquals("既に同一Emailは登録されています。", persistPage.tooltip(persistPage.userEmail));
    }
}
